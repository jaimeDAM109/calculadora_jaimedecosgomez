/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Calculadora_JaimeDeCos;

/**
 *
 * @author DAM109
 */
import java.util.Scanner;

public class CalculadoraExamen {

    static Scanner scanner = new Scanner(System.in);
    static int opcion = -1; //opción del menú
    static float numero1 = 0, numero2 = 0; //Variables de entrada

    public static void main(String[] args) {

        while (opcion != 0) {
            //Try catch para evitar que el programa termine si hay un error
            try {
                System.out.println("Elige opción:\n" + ""
                        + "1.- Sumar\n"
                        + "2.- Restar\n"
                        + "3.- Multiplicar\n"
                        + "4.- Dividir\n"
                        + "5.- Porcentaje"
                        + "0.- Salir");

                System.out.println("Selecciona una opción de 0 a 4");
                opcion = Integer.parseInt(scanner.nextLine());

                switch (opcion) {
                    case 1:
                        pideNumeros();
                        System.out.println(numero1 + " + " + numero2 + " = " + (numero1 + numero2));
                        break;
                    case 2:
                        pideNumeros();
                        System.out.println(numero1 + " - " + numero2 + " = " + (numero1 - numero2));
                        break;
                    case 3:
                        pideNumeros();
                        System.out.println(numero1 + " * " + numero2 + " = " + (numero1 * numero2));
                        break;
                    case 4:
                        pideNumeros();
                        System.out.println(numero1 + " / " + numero2 + " = " + (numero1 / numero2));
                        break;
                    case 5:
                        pideNumeros();
                        System.out.println(numero1 + " % de " + numero2 + " = " + ((numero1/100)*numero2));
                        break;
                    case 0:
                        System.out.println("Saliendo...");
                        break;
                    default:
                        System.out.println("Opción no disponible");
                        break;
                }

                System.out.println("\n");

            } catch (Exception e) {
                System.out.println("Error!");
            }
        }

    }

    //Método para recoger las variables de entrada
    public static void pideNumeros() {
        System.out.println("Introduce el primer número:");
        numero1 = Integer.parseInt(scanner.nextLine());

        System.out.println("Introduce el segundo número:");
        numero2 = Integer.parseInt(scanner.nextLine());

    }
}
